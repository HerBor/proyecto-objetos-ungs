import java.util.ArrayList;
import java.util.Iterator;

public class Deposito {
	
	private ArrayList<Paquete>paquetes;
	private boolean esFrio;
	private double capacidadMaxima;
		
	public Deposito(boolean esfrio, double capacidadMaxima) {
		this.esFrio = esfrio;
		this.capacidadMaxima = capacidadMaxima;
		this.paquetes = new ArrayList<Paquete>();
	}
	
	public boolean agregarPaquete(String destino, double peso, double volumen, boolean necesitaRefrigeracion) {
		boolean procesoExitoso = false;
		if (VerificarDisponibilidad (volumen) == true) {
			this.paquetes.add(new Paquete(destino, peso,  volumen, necesitaRefrigeracion));
			procesoExitoso = true;
		}
		return procesoExitoso;
	}
	
	public ArrayList<Paquete> seleccionarCarga(double pesoMaximo, double volumenMaximo, String destino){
		double pesoTotal = 0;
		double volumenTotal = 0;
		ArrayList <Paquete> carga = new ArrayList<Paquete>();
		ArrayList <Paquete> quedaEnDeposito = new ArrayList<Paquete>();
		Iterator <Paquete> it = this.getPaquetes().iterator();
		while(it.hasNext()) {
			Paquete paquete = it.next();
			double pesoPaquete = paquete.getPeso(); 
			double volumenPaquete = paquete.getVolumen(); 
			String destinoPaquete = paquete.getDestino();
			if (pesoTotal + pesoPaquete <= pesoMaximo && volumenTotal + volumenPaquete <= volumenMaximo && destinoPaquete == destino) {
				carga.add(paquete);
				pesoTotal = pesoTotal + pesoPaquete;
				volumenTotal = volumenTotal + volumenPaquete;
			} 
			else {
				quedaEnDeposito.add(paquete);
			}
		}
		this.paquetes = quedaEnDeposito;
		return carga;
	}
	
	private boolean VerificarDisponibilidad (double volumen) {
		boolean hayEspacioDisponible = false;
		double capacidadActual = 0;
		for (Paquete paquete : this.paquetes) {
			capacidadActual = capacidadActual + paquete.getVolumen();
			}
		if (capacidadActual + volumen <= this.capacidadMaxima){
			hayEspacioDisponible = true;
		}
		else{
			hayEspacioDisponible = false;
		}
		return hayEspacioDisponible;
	}

	public ArrayList<Paquete> getPaquetes() {
		return paquetes;
	}

	public void setPaquetes(ArrayList<Paquete> paquetes) {
		this.paquetes = paquetes;
	}

	public boolean isEsFrio() {
		return esFrio;
	}

	public void setEsFrio(boolean esFrio) {
		this.esFrio = esFrio;
	}

	public double getCapacidadMaxima() {
		return capacidadMaxima;
	}

	public void setCapacidadMaxima(double capacidadMaxima) {
		this.capacidadMaxima = capacidadMaxima;
	}
}
	
	

	
