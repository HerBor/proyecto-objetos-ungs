import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Empresa{
	private String cuit;
	private String nombre;
	private double capacidadDeCadaDeposito;
	private HashMap <String,Transporte> flota;
	private ArrayList <Viaje> viajes;
	private ArrayList <Deposito> depositos;

	// LISTO
	// Constructor de la empresa.
	Empresa(String cuit, String nombre, double capacidadDeCadaDeposito) {
		this.cuit = cuit;
		this.nombre = nombre;
		this.flota = new HashMap<String, Transporte>();
		this.viajes = new ArrayList<Viaje>();
		this.depositos = new ArrayList<Deposito>();
		this.capacidadDeCadaDeposito = capacidadDeCadaDeposito;
		Deposito depositoFrio = new Deposito(true, capacidadDeCadaDeposito);
		Deposito depositoNoFrio = new Deposito(false, capacidadDeCadaDeposito);
		this.depositos.add(depositoFrio);
		this.depositos.add(depositoNoFrio);
	}
	
	// LISTO
	// Incorpora un nuevo destino y su distancia en km.
	// Es requisito previo, para poder asignar un destino a un transporte.
	// Si ya existe el destino se debe generar una excepci�n.
	public void agregarDestino(String destino, int km) {
		verificarDestino(destino);
		viajes.add(new Viaje(destino, km));
	}

	private void verificarDestino(String destino) {
		for (Viaje viajes : viajes) {
			if (viajes.getDestino() == destino)
				throw new RuntimeException("El destino a agregar ya existe.");
		}
	}

	// LISTO
	// Los siguientes m�todos agregan los tres tipos de transportes a la
	// empresa, cada uno con sus atributos correspondientes.
	// La matr�cula funciona como identificador del transporte.
	public void agregarTrailer(String matricula, double cargaMax, double capacidad, boolean tieneRefrigeracion, double costoKm,
			double segCarga) {
		verificarTransporteEsNuevo(matricula);
		flota.put(matricula, new Trailer(matricula, cargaMax, capacidad, tieneRefrigeracion, costoKm, segCarga));
	}
	  
	public void agregarMegaTrailer(String matricula, double cargaMax, double capacidad, boolean tieneRefrigeracion,
			double costoKm, double segCarga, double costoFijo, double costoComida) {
		verificarTransporteEsNuevo(matricula);
		flota.put(matricula, new MegaTrailer(matricula, cargaMax, capacidad, tieneRefrigeracion, costoKm,
				segCarga, costoFijo, costoComida));
	}
    
	public void agregarFlete(String matricula, double cargaMax, double capacidad, double costoKm, int cantAcompaniantes,
			double costoPorAcompaniante) {
		verificarTransporteEsNuevo(matricula);
		flota.put(matricula,
				new Flete(matricula, cargaMax, capacidad, costoKm, cantAcompaniantes, costoPorAcompaniante));
	}
		
	private void verificarTransporteEsNuevo(String matricula) {
		if (flota.containsKey(matricula)) {
			throw new RuntimeException("El transporte a agregar ya existe.");
		}
	}

	// LISTO
	// Se asigna un destino a un transporte dada su matr�cula (el destino
	// debe haber sido agregado previamente, con el m�todo agregarDestino).
	// Si el destino no est� registrado, se debe generar una excepci�n.
	public void asignarDestino(String matricula, String destino) {
		verificarExisteDestino(destino);
		verificarExisteTransporte(matricula);
		asignarDestinoTransporte(matricula,destino);
	}
	
	private void verificarExisteDestino(String destino) {
		boolean existeDestino = false;
		for (Viaje viaje : viajes) {
			existeDestino = existeDestino || (viaje.getDestino() == destino);
		}
		if (!existeDestino) {
			throw new RuntimeException("El destino no ha sido ingresado");
		}
	}
	
	private void verificarExisteTransporte(String matricula) {
		if(!(flota.containsKey(matricula))) {
			throw new RuntimeException("La matricula no fue ingresada");
		}
	}

	private void asignarDestinoTransporte(String matricula, String destino) {
		Viaje viaje = buscarViaje(destino);
		Transporte transporte = buscarTransporte(matricula);
		if (transporte.tieneCarga()) {
			throw new RuntimeException("El transporte ya tiene cargamento, no se puede asignar destino");
		}
		asignarViaje(transporte,viaje);
	}
	
	private void asignarViaje(Transporte transporte, Viaje viaje) {
		transporte.verificarViaje(viaje.getDistancia());
		transporte.setDestino(viaje.getDestino());
		transporte.setKmdestino(viaje.getDistancia());
	}
	
	private Transporte buscarTransporte(String matricula) {
		Transporte transporteObtenido = null;
		for (Transporte transporte : flota.values()) {
			if (transporte.getIdentificacion() == matricula)
				transporteObtenido = transporte;
		}
		if (transporteObtenido == null) {
			throw new RuntimeException("El transporte con esta matr�cula no existe");
		}
		return transporteObtenido;
	}
	
	private Viaje buscarViaje(String destino) {
		Viaje viajeObtenido = null;
		for (Viaje viaje : viajes) {
			if (viaje.getDestino() == destino)
				viajeObtenido = viaje;
		}
		return viajeObtenido;
	}
	
	// LISTO
	// Se incorpora un paquete a alg�n dep�sito de la empresa.
	// Devuelve verdadero si se pudo incorporar, es decir,
	// si el dep�sito acorde al paquete tiene suficiente espacio disponible.
	public boolean incorporarPaquete(String destino, double peso, double volumen, boolean necesitaRefrigeracion) {
		Deposito deposito = buscarDeposito(necesitaRefrigeracion);
		boolean procesoExitoso = deposito.agregarPaquete(destino, peso, volumen, necesitaRefrigeracion);
		return procesoExitoso;
	}
	
	private Deposito buscarDeposito(boolean necesitaRefrigeracion) {
		Deposito depositoObtenido = null;
		for (Deposito deposito : this.depositos) {
			if (deposito.isEsFrio() == necesitaRefrigeracion) {
				depositoObtenido = deposito;
			}
		}
		return depositoObtenido;
	}
	
	// LISTO
	// Dado un ID de un transporte se pide cargarlo con toda la mercader�a
	// posible, de acuerdo al destino del transporte. No se debe permitir
	// la carga si est� en viaje o si no tiene asignado un destino.
	// Utilizar el dep�sito acorde para cargarlo.
	// Devuelve un double con el volumen de los paquetes subidos
	// al transporte.
	public double cargarTransporte(String matricula) {
		verificarExisteTransporte(matricula);
		double cargaTotal = cargar(matricula);
		return cargaTotal;
	}
	
	private double cargar(String matricula) {
		double cargaTotal;
		Transporte transporte = buscarTransporte(matricula);
		if (transporte.isEnViaje() || transporte.getDestino() == "" ) {
			throw new RuntimeException("El transporte no est� en condiciones para cargar");
		}
		Deposito deposito = buscarDeposito(transporte.esRefrigerado());
		cargaTotal = transporte.cargarDesdeDeposito(deposito);
		return cargaTotal;
	}

	// LISTO
	// Inicia el viaje del transporte identificado por la
	// matr�cula pasada por par�metro.
	// En caso de no tener mercader�a cargada o de ya estar en viaje
	// se genera una excepci�n.
	public void iniciarViaje(String matricula) {
		verificarExisteTransporte(matricula);
		cambiarEstadoViajeIniciado(matricula);
	}
	
	private void cambiarEstadoViajeIniciado(String matricula) {
		Transporte transporte=buscarTransporte(matricula);
		if(transporte.tieneCarga()==false || transporte.estaEnViaje()==true) {
			throw new RuntimeException("El transporte no puede iniciar un viaje");
		}
		else {
			transporte.cambiarEstadoEnViaje();
		}
	}
	
	// LISTO
	// Finaliza el viaje del transporte identificado por la
	// matr�cula pasada por par�metro.
	// El transporte vac�a su carga y blanquea su destino, para poder
	// ser vuelto a utilizar en otro viaje.
	// Genera excepci�n si no est� actualmente en viaje.
	public void finalizarViaje(String matricula) {
		verificarExisteTransporte(matricula);
		cambiarEstadoViajeFinalizado(matricula);
	}
	
	private void cambiarEstadoViajeFinalizado(String matricula) {
		Transporte transporte = buscarTransporte(matricula);
		if (transporte.estaEnViaje() == false) {
			throw new RuntimeException("El transporte no esta en viaje");
		}
		else {
			transporte.finalizarViaje();
			transporte.vaciarCarga();
		}
	}
	
	// LISTO
	// Obtiene el costo de viaje del transporte identificado por la
	// matr�cula pasada por par�metro.
	// Genera Excepci�n si el transporte no est� en viaje.
	public double obtenerCostoViaje(String matricula) {
		verificarExisteTransporte(matricula);
		double costoViaje = obtenerCostoViajeDeTransporte(matricula);
		return costoViaje;
	}
	
	private double obtenerCostoViajeDeTransporte(String matricula) {
		double costo = 0;
		Transporte transporte = buscarTransporte(matricula);
		if (transporte.estaEnViaje() == false) {
			throw new RuntimeException("El transporte no esta en viaje, no se puede obtener costo");
		} else {
			costo = transporte.obtenerCostoViaje();
		}
		return costo;
	}
	

	// LISTO
	// Busca si hay alg�n transporte igual en tipo, destino y carga.
	// En caso de que no se encuentre ninguno, se debe devolver null.
	
	

	public String obtenerTransporteIgual(String matricula) {
		verificarExisteTransporte(matricula);
		Transporte transporteClon = encontrarTransporteClon(matricula);
		if (transporteClon == null) {
			return null;
		}
		else {
			String matriculaClon = transporteClon.getIdentificacion().toString();
			return matriculaClon;
		}
	}

	private Transporte encontrarTransporteClon(String matricula) {
		Transporte transporteClon = null;
		Transporte transporte = buscarTransporte(matricula);
		Iterator<Transporte> it = flota.values().iterator();
		while (it.hasNext()) {
			Transporte transporteTemp = it.next();
			if (transporte.equals(transporteTemp) && transporteTemp.getIdentificacion() != matricula ) {
				transporteClon = transporteTemp;
			}
		}
		return transporteClon;
	}
	
	@Override
	public String toString() {
		StringBuilder texto = new StringBuilder();
		texto.append("Empresa [CUIT = ").append(this.cuit).append(", ");
		texto.append("Nombre = ").append(this.nombre).append(", ");
		texto.append("Capacidad de dep�sitos = ").append(this.capacidadDeCadaDeposito).append(", ");	
		texto.append("Cantidad de veh�culos = ").append(this.flota.size()).append("]");
		return texto.toString();
	}
}
