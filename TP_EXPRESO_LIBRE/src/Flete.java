public class Flete extends Transporte {
	private int cantAcompañantes;
	private double costoAcompañante;
	
	public Flete(String identificacion, double cargaMaxPeso, double cargaMaxVolumen,double costoKm,int acompañante,double costoAcompañante){
		super(identificacion,cargaMaxPeso,cargaMaxVolumen,costoKm);
		this.cantAcompañantes=acompañante;
		this.costoAcompañante=costoAcompañante;		
	}

	@Override
	public boolean esRefrigerado() {
		return false;
	}
	
	@Override
	public void verificarViaje(double kmViaje) {		
	}

	@Override
	public double obtenerCostoViaje() {
		double sumarCosto;
		sumarCosto=(this.getKmdestino()*this.getCostoPorKm())+(this.cantAcompañantes*this.costoAcompañante);
		return sumarCosto;
	}
	
	public int getCantAcompañantes() {
		return cantAcompañantes;
	}
	public void setCantAcompañantes(int cantAcompañantes) {
		this.cantAcompañantes = cantAcompañantes;
	}
	public double getCostoAcompañante() {
		return costoAcompañante;
	}
	public void setCostoAcompañante(double costoAcompañante) {
		this.costoAcompañante = costoAcompañante;
	}	
}
