

public class MegaTrailer extends Transporte {
	private double seguroDeCarga;
	private double gastoComida;
	private double costoViaje;
	private double kmMinimo;
	private boolean esRefrigerado;
	
	public MegaTrailer(String identificacion, double cargaMaxPeso, double cargaMaxVolumen, boolean esRefrigerado,double costoKm,double seguroCarga,double gastoComida,double costoViaje) {
		super(identificacion,cargaMaxPeso,cargaMaxVolumen,costoKm);
		this.gastoComida=gastoComida;
		this.costoViaje=costoViaje;
		this.esRefrigerado=esRefrigerado;
		this.seguroDeCarga=seguroCarga;
		this.kmMinimo = 500;
	}
	
	@Override
	public void verificarViaje(double kmViaje) {
		if (kmViaje < this.kmMinimo) {
			throw new RuntimeException("El transporte no est� habilitado para un viaje de menos de 500 km.");
		}
	}

	@Override
	public boolean esRefrigerado() {
		return this.esRefrigerado;
	}

	@Override
	public double obtenerCostoViaje() {
		double sumarCosto;
		sumarCosto=(this.getKmdestino()*this.getCostoPorKm()) + this.seguroDeCarga+ this.gastoComida+ this.costoViaje;
		return sumarCosto;
	}
	
	public double getSeguroDeCarga() {
		return seguroDeCarga;
	}
	public void setSeguroDeCarga(double seguroDeCarga) {
		this.seguroDeCarga = seguroDeCarga;
	}
	public double getGastoComida() {
		return gastoComida;
	}
	public void setGastoComida(double gastoComida) {
		this.gastoComida = gastoComida;
	}
	public double getCostoViaje() {
		return costoViaje;
	}
	public void setCostoViaje(double costoViaje) {
		this.costoViaje = costoViaje;
	}
	public double getKmMinimo() {
		return kmMinimo;
	}
	public void setKmMinimo(double kmMinimo) {
		this.kmMinimo = kmMinimo;
	}
	public boolean isEsRefrigerado() {
		return esRefrigerado;
	}
	public void setEsRefrigerado(boolean esRefrigerado) {
		this.esRefrigerado = esRefrigerado;
	}
}

