
public class Paquete {
	private double peso;
	private double volumen;
	private String destino;
	private boolean esFrio;	
	
	public Paquete (String destino, double peso, double volumen,  boolean esFrio) {
		this.peso = peso;
		this.volumen=volumen;
		this.destino=destino;
		this.esFrio=esFrio;	
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof Paquete) {
			Paquete OtroPaquete = (Paquete)obj;
			if(this.peso==OtroPaquete.getPeso() && this.volumen==OtroPaquete.getVolumen() && this.esFrio==OtroPaquete.isEsFrio()&& this.destino==OtroPaquete.getDestino()) {
				return true;
				
			}
			else
				return false;
		}
		else 
			return false;
		
		
	}
	
	

	public double getPeso() {
		return peso;
	}


	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getVolumen() {
		return volumen;
	}

	public void setVolumen(double volumen) {
		this.volumen = volumen;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public boolean isEsFrio() {
		return esFrio;
	}

	public void setEsFrio(boolean esFrio) {
		this.esFrio = esFrio;
	}
}