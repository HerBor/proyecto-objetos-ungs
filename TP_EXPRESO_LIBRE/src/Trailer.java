
public class Trailer extends Transporte {
	private double seguroDeCarga;
	private double kmMaximo;
	private boolean esRefrigerado;

	public Trailer(String identificacion, double cargaMaxPeso, double cargaMaxVolumen, boolean esRefrigerado,double costoKm,double seguroCarga){
		super(identificacion,cargaMaxPeso,cargaMaxVolumen,costoKm);
		this.seguroDeCarga=seguroCarga;
		this.kmMaximo=500;
		this.esRefrigerado=esRefrigerado;     
	}
	
	@Override
	public void verificarViaje(double kmViaje) {
		if (kmViaje > this.kmMaximo) {
			throw new RuntimeException("El transporte no est� habilitado para un viaje de m�s de 500 km.");
		}
	}

	@Override
	public boolean esRefrigerado() {
		return this.esRefrigerado;
	}

	@Override
	public double obtenerCostoViaje() {
		double sumarCosto;
		sumarCosto = (this.getKmdestino() * this.getCostoPorKm()) + this.seguroDeCarga;
		return sumarCosto;
	}

	public double getSeguroDeCarga() {
		return seguroDeCarga;
	}

	public void setSeguroDeCarga(double seguroDeCarga) {
		this.seguroDeCarga = seguroDeCarga;
	}

	public double getKmMaximo() {
		return kmMaximo;
	}

	public void setKmMaximo(double kmMaximo) {
		this.kmMaximo = kmMaximo;
	}

	public boolean isEsRefrigerado() {
		return esRefrigerado;
	}

	public void setEsRefrigerado(boolean esRefrigerado) {
		this.esRefrigerado = esRefrigerado;
	}
	
}

