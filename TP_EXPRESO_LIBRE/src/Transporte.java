import java.util.ArrayList;
import java.util.Iterator;

public abstract class Transporte {
	protected ArrayList<Paquete> carga;
	private String identificacion;
	private double cargaMaximaPeso;
	private double cargaMaximaVolumen;
	private double pesoActual;
	private double volumenActual;
	private double costoPorKm;
	private boolean enViaje;
	private String destino;
	private double kmdestino;
	
	

	public Transporte(String identificacion, double cargaMaxPeso, double cargaMaxVolumen,double costoKm) {
		this.carga = new ArrayList<Paquete>();
		this.identificacion = identificacion;
		this.cargaMaximaPeso = cargaMaxPeso;
		this.cargaMaximaVolumen = cargaMaxVolumen;
		this.pesoActual = 0;
		this.volumenActual = 0;
		this.costoPorKm = costoKm;
		this.enViaje = false;
		this.destino = "";
		this.kmdestino=0;
	}
	// F	
	public void finalizarViaje() {
		this.setEnViaje(false);		
	}
	
	public boolean tieneCarga() {
		if(this.getCarga().isEmpty())
		    return false;
		return true;
	}
		
	public double cargarDesdeDeposito(Deposito deposito) {
		double pesoMaximo = this.getCargaMaximaPeso();
		double volumenMaximo = this.getCargaMaximaVolumen();
		String destino = this.getDestino();
		ArrayList<Paquete> paquetesParaCargar = deposito.seleccionarCarga(pesoMaximo, volumenMaximo, destino);
		double volumenTotal = asignarCargaObtenerVolumen(paquetesParaCargar);
		return volumenTotal;
	}
	
	private double asignarCargaObtenerVolumen(ArrayList<Paquete> carga) {
		double pesoTotal = 0;
		double volumenTotal = 0;
		for (Paquete paquete : carga) {
			pesoTotal = pesoTotal + paquete.getPeso();
			volumenTotal = volumenTotal + paquete.getVolumen();
		}
		this.carga = carga;
		this.pesoActual = pesoTotal;
		this.volumenActual = volumenTotal;
		return volumenTotal;
	}
	
	public boolean tieneDestino() {
		if(this.getDestino()!="") {
			return true;
		}
		return false;
	}
	
	public boolean estaEnViaje() {
		if(this.isEnViaje()==true) {
			return true;
		}
		return false;
	}
	
	public void cambiarEstadoEnViaje() {
		this.setEnViaje(true);
	}
		
	public void vaciarCarga() {
		Iterator<Paquete> it = carga.iterator();
		while(it.hasNext()) {
			it.next();
			it.remove();
		}
		setearDestino();
	}
	
	private void setearDestino() {
		this.setDestino("");
		this.setKmdestino(0);	
	}
	
	public abstract double obtenerCostoViaje();
	
	public abstract boolean esRefrigerado();
	
	public abstract void verificarViaje(double kmViaje);

	public static boolean CompararCargas(ArrayList<Paquete> carga1, ArrayList<Paquete> carga2) {
		boolean estanTodos = true;
		ArrayList<Paquete> carga1Temp = carga1;
		ArrayList<Paquete> carga2Temp = carga2;
		if (carga1.size() == carga2Temp.size()) {
			Iterator<Paquete> iteradorCarga1 = carga1Temp.iterator();
			while (estanTodos && iteradorCarga1.hasNext()) {
				boolean paqueteCoincidencia = false;
				Paquete paquete1AChequear = iteradorCarga1.next();
				Iterator<Paquete>iteradorCarga2 = carga2Temp.iterator();
				while (iteradorCarga2.hasNext() && !paqueteCoincidencia) {
					Paquete paquete2AChequear = iteradorCarga2.next();
					if(paquete1AChequear.equals(paquete2AChequear)) {
						iteradorCarga1.remove();
						iteradorCarga2.remove();
					}
					paqueteCoincidencia = paqueteCoincidencia || paquete1AChequear.equals(paquete2AChequear);
				}
				estanTodos = estanTodos && paqueteCoincidencia;
			}
		}
		else {
			estanTodos = false;
		}
		return estanTodos;
	}
	
	public boolean equals(Object obj) {
		boolean esIgual = false;
		if (obj instanceof Transporte) {
			Transporte OtroTrans = (Transporte) obj;
			esIgual = CompararCargas(this.carga, OtroTrans.carga);
		}
		else {
			esIgual = false;
		}
		return esIgual;
	}
		
	
	
	
	
//	public boolean equals(Transporte transporte) {
//		boolean esIgual = false;
//		if(this.getClass() == transporte.getClass() && this.pesoActual == transporte.getPesoActual() && this.volumenActual == transporte.getVolumenActual() 
//				&& this.destino == transporte.getDestino() && this.esRefrigerado() == transporte.esRefrigerado() 
//				&& this.getCarga().size() == transporte.getCarga().size()) {
//			esIgual = true;
//		}
//		return esIgual;
//	}

	public ArrayList<Paquete> getCarga() {
		return carga;
	}

	public void setCarga(ArrayList<Paquete> carga) {
		this.carga = carga;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public double getCargaMaximaPeso() {
		return cargaMaximaPeso;
	}

	public void setCargaMaximaPeso(double cargaMaximaPeso) {
		this.cargaMaximaPeso = cargaMaximaPeso;
	}

	public double getCargaMaximaVolumen() {
		return cargaMaximaVolumen;
	}

	public void setCargaMaximaVolumen(double cargaMaximaVolumen) {
		this.cargaMaximaVolumen = cargaMaximaVolumen;
	}

	public double getPesoActual() {
		return pesoActual;
	}

	public void setPesoActual(double pesoActual) {
		this.pesoActual = pesoActual;
	}

	public double getVolumenActual() {
		return volumenActual;
	}

	public void setVolumenActual(double volumenActual) {
		this.volumenActual = volumenActual;
	}

	public double getCostoPorKm() {
		return costoPorKm;
	}

	public void setCostoPorKm(double costoPorKm) {
		this.costoPorKm = costoPorKm;
	}

	public boolean isEnViaje() {
		return enViaje;
	}

	public void setEnViaje(boolean enViaje) {
		this.enViaje = enViaje;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public double getKmdestino() {
		return kmdestino;
	}

	public void setKmdestino(double kmdestino) {
		this.kmdestino = kmdestino;
	}
}


