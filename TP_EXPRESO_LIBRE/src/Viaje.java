
public class Viaje {
	private String destino;
	private double distancia;
	
	
	public Viaje(String destino,double distancia) {	
		this.destino=destino;
		this.distancia=distancia;	
	}


	public String getDestino() {
		return destino;
	}


	public void setDestino(String destino) {
		this.destino = destino;
	}


	public double getDistancia() {
		return distancia;
	}


	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}
	
}
