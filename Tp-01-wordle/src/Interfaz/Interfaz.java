package Interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.ComponentOrientation;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextArea;
import javax.swing.JTable;
import javax.swing.JSeparator;

public class Interfaz {

	private JFrame frame;
	private JTextField palabra1Letra1;
	private JTextField palabra1Letra2;
	private JTextField palabra1Letra4;
	private JTextField palabra1Letra3;
	private JTextField palabra1Letra5;
	private JTextField palabra2Letra1;
	private JTextField palabra2Letra2;
	private JTextField palabra2Letra3;
	private JTextField palabra2Letra4;
	private JTextField palabra2Letra5;
	private JTextField palabra3Letra1;
	private JTextField palabra3Letra2;
	private JTextField palabra3Letra3;
	private JTextField palabra3Letra4;
	private JTextField palabra3Letra5;
	private JTextField palabra4Letra1;
	private JTextField palabra4Letra2;
	private JTextField palabra4Letra3;
	private JTextField palabra4Letra4;
	private JTextField palabra4Letra5;
	private JTextField palabra5Letra1;
	private JTextField palabra5Letra2;
	private JTextField palabra5Letra3;
	private JTextField palabra5Letra4;
	private JTextField palabra5Letra5;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		// look and feel (cambia por defecto el tema predeteminado de windows )
		try 
		{UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());}
		
		catch(Exception e)
		{}
			
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz window = new Interfaz(); // crear y hacerlo visible
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interfaz() 
	{		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		frame.setBounds(100, 100, 467, 463);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		palabra1Letra1 = new JTextField();
		palabra1Letra1.setColumns(10);
		palabra1Letra1.setBounds(51, 50, 53, 44);
		frame.getContentPane().add(palabra1Letra1);
		
		palabra1Letra2 = new JTextField();
		palabra1Letra2.setColumns(10);
		palabra1Letra2.setBounds(116, 50, 53, 44);
		frame.getContentPane().add(palabra1Letra2);
		
		palabra1Letra3 = new JTextField();
		palabra1Letra3.setColumns(10);
		palabra1Letra3.setBounds(179, 50, 53, 44);
		frame.getContentPane().add(palabra1Letra3);
		
		palabra1Letra4 = new JTextField();
		palabra1Letra4.setColumns(10);
		palabra1Letra4.setBounds(242, 50, 53, 44);
		frame.getContentPane().add(palabra1Letra4);
		
		palabra1Letra5 = new JTextField();
		palabra1Letra5.setColumns(10);
		palabra1Letra5.setBounds(319, 50, 53, 44);
		frame.getContentPane().add(palabra1Letra5);
		
		
		palabra2Letra1 = new JTextField();
		palabra2Letra1.setColumns(10);
		palabra2Letra1.setBounds(51, 117, 53, 44);
		frame.getContentPane().add(palabra2Letra1);
		
		palabra2Letra2 = new JTextField();
		palabra2Letra2.setColumns(10);
		palabra2Letra2.setBounds(116, 117, 53, 44);
		frame.getContentPane().add(palabra2Letra2);
		
		palabra2Letra3 = new JTextField();
		palabra2Letra3.setColumns(10);
		palabra2Letra3.setBounds(179, 117, 53, 44);
		frame.getContentPane().add(palabra2Letra3);
		
		palabra2Letra4 = new JTextField();
		palabra2Letra4.setColumns(10);
		palabra2Letra4.setBounds(242, 117, 53, 44);
		frame.getContentPane().add(palabra2Letra4);
		
		palabra2Letra5 = new JTextField();
		palabra2Letra5.setColumns(10);
		palabra2Letra5.setBounds(319, 117, 53, 44);
		frame.getContentPane().add(palabra2Letra5);
		
		palabra3Letra1 = new JTextField();
		palabra3Letra1.setColumns(10);
		palabra3Letra1.setBounds(51, 173, 53, 44);
		frame.getContentPane().add(palabra3Letra1);
		
		palabra3Letra2 = new JTextField();
		palabra3Letra2.setColumns(10);
		palabra3Letra2.setBounds(116, 173, 53, 44);
		frame.getContentPane().add(palabra3Letra2);
		
		palabra3Letra3 = new JTextField();
		palabra3Letra3.setColumns(10);
		palabra3Letra3.setBounds(179, 173, 53, 44);
		frame.getContentPane().add(palabra3Letra3);
		
		palabra3Letra4 = new JTextField();
		palabra3Letra4.setColumns(10);
		palabra3Letra4.setBounds(242, 173, 53, 44);
		frame.getContentPane().add(palabra3Letra4);
		
		palabra3Letra5 = new JTextField();
		palabra3Letra5.setColumns(10);
		palabra3Letra5.setBounds(319, 173, 53, 44);
		frame.getContentPane().add(palabra3Letra5);
		
		palabra4Letra1 = new JTextField();
		palabra4Letra1.setColumns(10);
		palabra4Letra1.setBounds(51, 234, 53, 44);
		frame.getContentPane().add(palabra4Letra1);
		
		palabra4Letra2 = new JTextField();
		palabra4Letra2.setColumns(10);
		palabra4Letra2.setBounds(116, 234, 53, 44);
		frame.getContentPane().add(palabra4Letra2);
		
		palabra4Letra3 = new JTextField();
		palabra4Letra3.setColumns(10);
		palabra4Letra3.setBounds(179, 234, 53, 44);
		frame.getContentPane().add(palabra4Letra3);
		
		palabra4Letra4 = new JTextField();
		palabra4Letra4.setColumns(10);
		palabra4Letra4.setBounds(242, 234, 53, 44);
		frame.getContentPane().add(palabra4Letra4);
		
		palabra4Letra5 = new JTextField();
		palabra4Letra5.setColumns(10);
		palabra4Letra5.setBounds(319, 234, 53, 44);
		frame.getContentPane().add(palabra4Letra5);
		
		palabra5Letra1 = new JTextField();
		palabra5Letra1.setColumns(10);
		palabra5Letra1.setBounds(51, 300, 53, 44);
		frame.getContentPane().add(palabra5Letra1);
		
		palabra5Letra2 = new JTextField();
		palabra5Letra2.setColumns(10);
		palabra5Letra2.setBounds(116, 300, 53, 44);
		frame.getContentPane().add(palabra5Letra2);
		
		palabra5Letra3 = new JTextField();
		palabra5Letra3.setColumns(10);
		palabra5Letra3.setBounds(179, 300, 53, 44);
		frame.getContentPane().add(palabra5Letra3);
		
		palabra5Letra4 = new JTextField();
		palabra5Letra4.setColumns(10);
		palabra5Letra4.setBounds(242, 300, 53, 44);
		frame.getContentPane().add(palabra5Letra4);
		
		palabra5Letra5 = new JTextField();
		palabra5Letra5.setColumns(10);
		palabra5Letra5.setBounds(319, 300, 53, 44);
		frame.getContentPane().add(palabra5Letra5);
		
		Juego.ocultarLinea(palabra2Letra1, palabra2Letra2, palabra2Letra3, palabra2Letra4, palabra2Letra5);
		Juego.ocultarLinea(palabra3Letra1, palabra3Letra2, palabra3Letra3, palabra3Letra4, palabra3Letra5);
		Juego.ocultarLinea(palabra4Letra1, palabra4Letra2, palabra4Letra3, palabra4Letra4, palabra4Letra5);
		Juego.ocultarLinea(palabra5Letra1, palabra5Letra2, palabra5Letra3, palabra5Letra4, palabra5Letra5);

		
		JButton verificadorBoton = new JButton("Enter");
		verificadorBoton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				palabra2Letra1.requestFocus();
			}
		});
		verificadorBoton.setBounds(179, 390, 89, 23);
		verificadorBoton.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent e) {
				
				int contador = 1;				
				String palabraEscrita = Juego.extraerTexto(palabra1Letra1, palabra1Letra2, palabra1Letra3, palabra1Letra4, palabra1Letra5);			
				boolean esCorrecto1 = Juego.esPalabraCorrecta (palabraEscrita);
				
				
				if(esCorrecto1 == true) {
					palabra1Letra1.setBackground(Color.GREEN);
					palabra1Letra2.setBackground(Color.GREEN);
					palabra1Letra3.setBackground(Color.GREEN);
					palabra1Letra4.setBackground(Color.GREEN);
					palabra1Letra5.setBackground(Color.GREEN);
				}
				else {
					Juego.mostrarSiguienteLinea(palabra2Letra1, palabra2Letra2, palabra2Letra3, palabra2Letra4, palabra2Letra5);
					contador ++;

					
					//HACER QUEPAREZCAN LOS CUADROS SIGUIENTES
				}
				
				
				
		
				
				
			}
		});
		
		
		
		
		frame.getContentPane().add(verificadorBoton);
		
		JLabel lblNewLabel = new JLabel("Wordle");
		lblNewLabel.setBounds(157, 11, 98, 21);
		lblNewLabel.setFont(new Font("Source Serif Pro Black", Font.BOLD | Font.ITALIC, 25));
		lblNewLabel.setBackground(Color.WHITE);
		frame.getContentPane().add(lblNewLabel);
		

		

		
		
	}
}
